---
title: "DevOps careers: SRE, engineer, and platform engineer"
author: Lauren Gibbons Paul
author_twitter: gitlab
categories: devops platform
tags: DevOps, careers, engineering
description: Where does an SRE leave off and a DevOps engineer (or platform
  engineer) begin? Here's what you need to know.
image_title: /images/blogimages/comparing-confusing-terms-in-github-bitbucket-and-gitlab-cover.jpg
twitter_text: Should your next DevOps career move be to SRE or DevOps engineer?
  Or maybe it's time to move to DevOps platform engineer? Here's what you need
  to know about these roles.
---

Even if you’re totally happy in your current position, it pays to keep an eye on your DevOps career path and learn about emerging roles, especially given [the way the DevOps space evolves so rapidly](https://www.simplilearn.com/is-a-devops-career-right-for-you-article). 

For example, you might be wondering about the role of site reliability engineer (SRE) as opposed to DevOps engineer (and the totally new position called DevOps platform engineer, more on that later). These are all engineering positions requiring tech expertise and coding chops, but they play distinct roles on the DevOps team. Here’s what you need to know:

## SRE: A seasoned role

As the title suggests, at a high level, SREs focus primarily on reliability, solving operational, scale, and uptime problems. In 2003, Google originated the SRE role to safeguard the uptime of its site, but it has evolved considerably since the advent of cloud native applications and platforms. Today, SREs concentrate on [minimizing the frequency and impact of failures](https://thenewstack.io/the-evolution-of-the-site-reliability-engineer-sre/) that can impact the overall reliability of a cloud
application. 

According to Glassdoor, SREs typically require a Bachelor’s or graduate engineering or computer science degree. Salaries range widely, according to Glassdoor, hitting about $120,000 after 2 to 4 years of experience but can reach up to [$300,000 and higher](https://www.glassdoor.com/Salaries/us-site-reliability-engineer-salary-SRCH_IL.0,2_IN1_KO3,28.html) at the senior level.

At least one blogger feels [the SRE title](https://rootly.com/blog/should-you-be-an-sre-or-a-devops-engineer) carries more prestige and earning potential than DevOps engineers.

Typical SRE responsibilities include everything from designing, developing, installing, and maintaining software solutions to working with engineering teams to refine deployment and release processes. Collaboration and communication are important job skills for the SRE role, as they need to work closely with multiple roles across the organization. At the time of this blog's publication, there were 4,000 SRE jobs on Glassdoor. Indeed had more than 5,000 SRE postings and ZipRecruiter showed [nearly 12,000 posts](https://www.ziprecruiter.com/candidate/search?radius=5000&amp;search=site+reliability+engineer&amp;location=Remote) for remote SRE jobs.

Python, Go, and Java were the [most sought-after SRE skills](https://www.indeed.com/jobs=site%20Reliability%20Engineer&amp;l&amp;vjk=829f6081218e60bd) listed on Indeed.

According to Indeed, SREs transition to "DevOps engineer" at a high rate.

## DevOps engineers bridge the gap

DevOps engineers, on the other hand, concentrate on removing obstacles to production and automation and [making development and IT work well together](https://harness.io/blog/sre-vs-devops/).

Like SREs, DevOps engineers need to be good at working and communicating with others, eliminating barriers to increase speed and quality of code delivery. With typically less need to be on call, the DevOps engineer
may have a more favorable work-life balance than an SRE, who can have around-the-clock call.

DevOps engineer work responsibilities include such things as analysis of technology utilized within the company and then developing steps and processes to improve and expand upon them. Project management is another key function, establishing milestones for departmental contributions and establishing processes to facilitate collaboration.

The educational requirements for the two roles are comparable, with a Bachelor’s degree in computer science or engineering or higher as the usual price of admission.

According to Glassdoor, the salary range for DevOps engineers is slightly lower than that of SREs, from a low of about $63,000 up to a high of $234,000 for someone with [2 to 4 years of experience](https://www.glassdoor.com/Salaries/us-devops-engineer-salary-SRCH_IL.0,2_IN1_KO3,18.htm). 

DevOps engineer positions are easier to find than SREs. Glassdoor has more than 6,000 DevOps engineer job posts. Indeed has more than 17,000. And ZipRecruiter has [more than 81,000](https://www.ziprecruiter.com/candidate/search?radius=5000&amp;search=devops+engineer&amp;location=Remote) remote DevOps engineer listings.

## New to the game

Cloud native development and the desire to have a unified DevOps platform have brought a new role, the DevOps platform engineer, a position that [works in parallel with the site reliability engineering function](/topics/devops/what-is-a-devops-platform-engineer/).

Platform engineering teams apply development principles to accelerate software delivery, ensuring app dev teams are productive in all aspects of the lifecycle. Platform engineers focus on the entire software development lifecycle from source to production. From this introspective process, they build a workflow that enables application
developers to [rapidly code and ship software](https://www.getambassador.io/resources/rise-of-cloud-native-engineering-organizations/).

You can find a helpful description of the roles of SRE vs. DevOps engineer vs. platform engineer [here](https://iximiuz.com/en/posts/devops-sre-and-platform-engineering/).
 
But it’s hard to find much career data for this emerging role. Glassdoor, Indeed, and ZipRecruiter do not yet separate out this role from the category of “DevOps engineer,” and consolidated salary and career path data is not available at this time. It is reasonable to conclude this new role will have higher pay based on rarer skill sets and job experience. Suffice to say, this is a hot area and bears watching.
